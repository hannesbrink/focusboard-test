=== Plugin Name ===
Contributors: Hannes Brink
Donate link: https://www.bitbrighter.com
Tags: comments, spam
Requires at least: 3.0.1
Tested up to: 5.4
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Simple MessageBoard created as a Focus Exercise.

== Description ==

Simple MessageBoard created as a Focus Exercise.

== Installation ==

1. Upload `focusboard.zip` via the plugin uploader provided by WordPress.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Import the `starting-export.xml` found on the base folder of the plugin, use the default WordPress importer plugin.
4. Browse to `www.yourdomain.com/focus-messages/` to view the message board.

