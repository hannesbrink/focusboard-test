<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.bitbrighter.com
 * @since      1.0.0
 *
 * @package    Focusboard
 * @subpackage Focusboard/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Focusboard
 * @subpackage Focusboard/admin
 * @author     Hannes Brink <hannesbrink@gmail.com>
 */
class Focusboard_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Focusboard_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Focusboard_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/focusboard-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Focusboard_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Focusboard_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/focusboard-admin.js', array( 'jquery' ), $this->version, false );

	}


	/**
	 * Register the message custom post type.
	 *
	 * @since    1.0.0
	 */
	public function focusboard_post_types(){

		// REGISTER MESSAGEBOARD TYPE
		// Set UI labels for Custom Post Type
		$labels = array(
			'name'                => _x( 'Messages', 'Post Type General Name', 'focusboard' ),
			'singular_name'       => _x( 'Message', 'Post Type Singular Name', 'focusboard' ),
			'menu_name'           => __( 'Messages', 'focusboard' ),
			'parent_item_colon'   => __( 'Parent Message', 'focusboard' ),
			'all_items'           => __( 'All Messages', 'focusboard' ),
			'view_item'           => __( 'View Message', 'focusboard' ),
			'add_new_item'        => __( 'Add New Message', 'focusboard' ),
			'add_new'             => __( 'Add New Message', 'focusboard' ),
			'edit_item'           => __( 'Edit Message', 'focusboard' ),
			'update_item'         => __( 'Update Message', 'focusboard' ),
			'search_items'        => __( 'Search Message', 'focusboard' ),
			'not_found'           => __( 'Not Found', 'focusboard' ),
			'not_found_in_trash'  => __( 'Not found in Trash', 'focusboard' ),
		);
			
		// Set other options for Custom Post Type
		$args = array(
			'label'               => __( 'Messages', 'focusboard' ),
			'description'         => __( '', 'focusboard' ),
			'labels'              => $labels,
			
			'supports'            => array( 
				'title', 
				'editor', 
				//'excerpt', 
				'author', 
				'thumbnail', 
				//'custom-fields', 
				'comments',
			),
			
			
			/* A hierarchical CPT is like Pages and can have
			* Parent and child items. A non-hierarchical CPT
			* is like Posts.
			*/ 
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'menu_icon'           => 'data:image/svg+xml;base64,PHN2ZyBhcmlhLWhpZGRlbj0idHJ1ZSIgZm9jdXNhYmxlPSJmYWxzZSIgZGF0YS1wcmVmaXg9ImZhZCIgZGF0YS1pY29uPSJjb21tZW50LWFsdC1lZGl0IiBjbGFzcz0ic3ZnLWlubGluZS0tZmEgZmEtY29tbWVudC1hbHQtZWRpdCBmYS13LTE2IiByb2xlPSJpbWciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmlld0JveD0iMCAwIDUxMiA1MTIiPjxnIGNsYXNzPSJmYS1ncm91cCI+PHBhdGggY2xhc3M9ImZhLXNlY29uZGFyeSIgZmlsbD0iY3VycmVudENvbG9yIiBkPSJNNDQ4IDBINjRBNjQuMDYgNjQuMDYgMCAwIDAgMCA2NHYyODhhNjQuMDYgNjQuMDYgMCAwIDAgNjQgNjRoOTZ2ODRhMTIgMTIgMCAwIDAgMTkuMSA5LjdMMzA0IDQxNmgxNDRhNjQuMDYgNjQuMDYgMCAwIDAgNjQtNjRWNjRhNjQuMDYgNjQuMDYgMCAwIDAtNjQtNjR6TTIxNS40IDMxMC42bC00OC4yIDUuNGExMC4xNyAxMC4xNyAwIDAgMS0xMS4yLTExLjJsNS40LTQ4LjIgOTYuMy05Ni4zIDU0IDU0em0xNTAuNy0xNTAuN2wtMzEuOCAzMS44LTU0LTU0IDMxLjgtMzEuOGEyMC4yMiAyMC4yMiAwIDAgMSAyOC42IDBsMjUuNCAyNS40YTIwLjIyIDIwLjIyIDAgMCAxIDAgMjguNnoiIG9wYWNpdHk9IjAuNCI+PC9wYXRoPjxwYXRoIGNsYXNzPSJmYS1wcmltYXJ5IiBmaWxsPSJjdXJyZW50Q29sb3IiIGQ9Ik0xNjEuNCAyNTYuNmwtNS40IDQ4LjJhMTAuMTcgMTAuMTcgMCAwIDAgMTEuMiAxMS4ybDQ4LjItNS40IDk2LjMtOTYuMy01NC01NHptMjA0LjctMTI1LjNsLTI1LjQtMjUuNGEyMC4yMiAyMC4yMiAwIDAgMC0yOC42IDBsLTMxLjggMzEuOCA1NCA1NCAzMS44LTMxLjhhMjAuMjIgMjAuMjIgMCAwIDAgMC0yOC42eiI+PC9wYXRoPjwvZz48L3N2Zz4=',
			'can_export'          => true,
			'has_archive'         => true,
			'query_var'           => true,
			'exclude_from_search' => false,
			'rewrite'             => array( 'slug' => 'focus-messages' ),
			'publicly_queryable'  => true,
		);
			
		// Registering your Custom Post Type
		register_post_type( 'message', $args );

		add_post_type_support( 'message', 'comments' );

		flush_rewrite_rules();
	}



}
