# README #


### What is this repository for? ###

* Simple MessageBoard created as a Focus Exercise.
* 1.0.0


### How do I get set up? ###

1. Upload `focusboard.zip` via the plugin uploader provided by WordPress.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Import the `starting-export.xml` found on the base folder of the plugin, use the default WordPress importer plugin.
4. Browse to `www.yourdomain.com/focus-messages/` to view the message board.

