<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.bitbrighter.com
 * @since      1.0.0
 *
 * @package    Focusboard
 * @subpackage Focusboard/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Focusboard
 * @subpackage Focusboard/includes
 * @author     Hannes Brink <hannesbrink@gmail.com>
 */
class Focusboard_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		$pageObject = get_page_by_path(sanitize_title('Focus Login'));
		if(!$pageObject):
			wp_insert_post( array(
				'post_type'		=> 'page',
				'post_title'    => 'Focus Login',
				'post_content'  => '[focusboard_login]',
				'post_status'   => 'publish',
				'post_author'   => 1,
			) );
		endif;

		/*$pageObject = get_page_by_path(sanitize_title('Focus Message Board'));
		if(!$pageObject):
			wp_insert_post( array(
				'post_type'		=> 'page',
				'post_title'    => 'Focus Message Board',
				'post_content'  => '[focusboard_messages]',
				'post_status'   => 'publish',
				'post_author'   => 1,
			) );
		endif;*/

	}


}
