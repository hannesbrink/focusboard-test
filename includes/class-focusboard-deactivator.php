<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.bitbrighter.com
 * @since      1.0.0
 *
 * @package    Focusboard
 * @subpackage Focusboard/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Focusboard
 * @subpackage Focusboard/includes
 * @author     Hannes Brink <hannesbrink@gmail.com>
 */
class Focusboard_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

		$pageObject = get_page_by_path(sanitize_title('Focus Login'));
		if($pageObject):
			wp_delete_post($pageObject->ID, true);
		endif;

		/*$pageObject = get_page_by_path(sanitize_title('Focus Message Board'));
		if($pageObject):
			wp_delete_post($pageObject->ID, true);
		endif;*/

		flush_rewrite_rules();

	}

}
