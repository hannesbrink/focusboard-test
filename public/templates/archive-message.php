
<?php

get_header();

$withcomments = 1;

$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

$args = array(
	'post_type' => 'message',
	'posts_per_page' => 2,
	'paged' => $paged
);

$the_query = new WP_Query( $args );

?>

<div id="focusboard-messages">

	<div class="container">
		
		<?php if(is_user_logged_in()): // show add new message form if user is logged in. ?>
		<?= do_shortcode('[focusboard_new_message_form]'); ?>
		<?php endif; ?>

		<div id="content-area" class="clearfix">
			
			<?php
				
				// The Loop
				if ( $the_query->have_posts() ) {
					while ( $the_query->have_posts() ) {
						$the_query->the_post();
						require( plugin_dir_path( __FILE__ ).'/includes/message.php' );
					}
				} else {
					echo "No messages have been posted.";
				}

			?>

			<div class="focusboard-pagination">
				<?php 
					echo paginate_links( array(
						'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
						'total'        => $the_query->max_num_pages,
						'current'      => max( 1, get_query_var( 'paged' ) ),
						'format'       => '?paged=%#%',
						'show_all'     => false,
						'type'         => 'plain',
						'end_size'     => 2,
						'mid_size'     => 1,
						'prev_next'    => true,
						'prev_text'    => sprintf( '<i></i> %1$s', __( 'Previous', 'text-domain' ) ),
						'next_text'    => sprintf( '%1$s <i></i>', __( 'Next', 'text-domain' ) ),
						'add_args'     => false,
						'add_fragment' => '',
					) );
				?>
			</div><!-- .pagination -->

			<?php 

			/* Restore original Post Data */
			wp_reset_postdata();

			?>

		</div> <!-- #content-area -->

	</div> <!-- .container -->

</div> <!-- #focusboard-messages -->

<?php

get_footer();

?>
