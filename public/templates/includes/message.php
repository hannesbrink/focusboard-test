<article id="post-<?php the_ID(); ?>" <?php post_class('focusboard-message'); ?>>


	<h2 class="article-title main_title">
		<?php if(!is_singular('message')): ?>
			<a href='<?= get_permalink(get_the_ID()); ?>'><?php the_title(); ?></a>
		<?php else: ?>
			<?php the_title(); ?>
		<?php endif; ?>
	</h2>

	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
	<?php
		if(isset($image[0])):
			?><div class="post-image"><img src="<?= $image[0]; ?>" width="100%"><?php 
		endif;
	?>

	<?php $terms = get_the_terms( get_the_ID(), 'messageboard' ); ?>
	<?php if(!is_wp_error($terms)): ?>
	<div class="post-terms">
		<?php foreach($terms as $term): ?>
		<a class="post-term" href='<?= get_term_link($term->term_id) ?>'><?= $term->name; ?></a>
		<?php endforeach; ?>
	</div>
	<?php endif; ?>

	<div class="article-content">
		<?php
		the_content();
		wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
		?>
	</div> <!-- .entry-content -->

	<?php
		comments_template();
	?>

</article> <!-- .focusboard-message -->
