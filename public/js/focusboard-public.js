(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */


	$(document).ready(function(){

		$('#focusboard-new-message-form').on('submit', function(e){

			e.preventDefault();

			var formData = $( this ).serializeArray();

			$(this).fadeOut('fast', function(){

				$('.focusboard-new-message-loading').fadeIn('fast', function(){

					$.post(
						ajax.url, 
						{
							'action': 'add_message',
							'data':  formData
						}, 
						function(response) {
							
							if(response != false){

								var messagedata = JSON.parse(response);


								$('.focusboard-new-message-loading').fadeOut('fast', function(){

									$('.focusboard-success-screen').fadeIn('fast', function(){

										setTimeout(function(){

											$('.focusboard-success-screen').fadeOut('fast', function(){

												$('#focusboard-new-message-form').fadeIn('fast');

												var html = '<article id="post-'+messagedata.id+'" class="focusboard-message post-'+messagedata.id+' message type-message status-publish hentry">';
												html += '<h2 class="article-title main_title"><a href="'+messagedata.link+'">'+messagedata.title+'</a></h2>';
												html += '<div class="article-content">'+messagedata.desc+'</div> <!-- .entry-content -->';

												html += '<div id="respond">';
													
												html += '<h3>Leave a Reply</h3>';
														
												html += '<div class="cancel-comment-reply"><small><a rel="nofollow" id="cancel-comment-reply-link" href="/focus-messages/#respond" style="display:none;">Click here to cancel reply.</a></small></div>';
														
																
												html += '<form action="http://focus.test/wp-comments-post.php" method="post" id="commentform">';
												html += '<p>Logged in as <a href="http://focus.test/wp-admin/profile.php">fb-admin</a>. <a href="http://focus.test/wp-login.php?action=logout&amp;redirect_to=http%3A%2F%2Ffocus.test%2Ffocus-messages%2Fdemo-message-two%2F&amp;_wpnonce=f7404eeadc" title="Log out of this account">Log out »</a></p>';
												html += '<p><textarea name="comment" id="comment" cols="100%" rows="10" tabindex="4"></textarea></p>';
												html += '<p><input name="submit" type="submit" id="submit" tabindex="5" value="Submit Comment">';
												html += '<input type="hidden" name="comment_post_ID" value="'+messagedata.id+'" id="comment_post_ID">';
												html += '<input type="hidden" name="comment_parent" id="comment_parent" value="0">';
												html += '</p>';
														
												html += '</form>';
														
												html += '</div>';

												html += '</article> <!-- .focusboard-message -->';

												$('#focusboard-messages #content-area').prepend(html);

											});

										}, 1000);

									});
								});



							} else {

							}
						}
					);


				});


			});


		});

	});

})( jQuery );
