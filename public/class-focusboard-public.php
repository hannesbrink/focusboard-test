<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.bitbrighter.com
 * @since      1.0.0
 *
 * @package    Focusboard
 * @subpackage Focusboard/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Focusboard
 * @subpackage Focusboard/public
 * @author     Hannes Brink <hannesbrink@gmail.com>
 */
class Focusboard_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		add_shortcode( 'focusboard_login', array( $this, 'focusboard_login_function' ) );
		add_shortcode( 'focusboard_new_message_form', array( $this, 'focusboard_new_message_form_function' ) );

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Focusboard_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Focusboard_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */


		wp_enqueue_style( $this->plugin_name.'-boostrap', plugin_dir_url( __FILE__ ) . 'css/bootstrap.min.css', array(), '4.3.1', 'all' );
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/focusboard-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Focusboard_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Focusboard_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		//if ( is_singular('message') ): wp_enqueue_script( 'comment-reply' ); endif;

		wp_enqueue_script( $this->plugin_name.'-boostrap-js', plugin_dir_url( __FILE__ ) . 'js/bootstrap.bundle.min.js', array( 'jquery' ), '4.3.1', false );


		wp_register_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/focusboard-public.js', array( 'jquery' ), $this->version, false );
		wp_localize_script( $this->plugin_name, 'ajax', array( 'url' => admin_url( 'admin-ajax.php' ) ) );
		wp_enqueue_script( $this->plugin_name );
	}

	/**
	 * set pagination page values and post_per_page of the custom post type archive page.
	 *
	 * @param	array		$query
	 * @since    1.0.0
	 */
	public function focusboard_override_messages_query( $query ){

		if(!is_admin() && is_archive('message')){

			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			$query->query_vars['posts_per_page'] = 3;
			$query->query_vars['paged'] = $paged;

		}

	}

	/**
	 * Whena form is submitted capture and process the form's POST data.
	 *
	 * @since    1.0.0
	 */
	public function focusboard_capture_forms(){

        if( isset($_POST['fbl_submit']) ):

            $rem = (isset($_POST['fbl_remember']) && $_POST['fbl_remember'] == 'on')? true: false;

            $creds = array(
                'user_login'    => $_POST['fbl_emailaddress'],
                'user_password' => $_POST['fbl_password'],
                'remember'      => $rem,
            );

            //error_log(print_r($creds, true));

            $user = wp_signon( $creds, false );
            if ( is_wp_error( $user ) ) {
                error_log( $user->get_error_message() );
            } else {
                if($rem){
                    wp_set_auth_cookie( $user->ID, 1, is_ssl() );
                } else {
                    wp_set_auth_cookie( $user->ID, 0, is_ssl() );
                }
                $redirect_to = (isset($_POST['fbl_redirect_to']))? $_POST['fbl_redirect_to']: home_url();
                wp_safe_redirect( $redirect_to );
                exit();
            }

        endif;       

	}

	/**
	 * markup and code for the [focusboard_login] shortcode.
	 *
	 * @param	array		$atts
	 * @param	string		$content
	 * @since    1.0.0
	 */
	public static function focusboard_login_function( $atts, $content = "" ){
		ob_start();

		?>
		<div class='focusboard-login'>

			<form id='focusboard-login-form' action="" method="post">

				<?php if(isset($_GET['redirect_to'])): ?>
					<input type='hidden' name='fbl_redirect_to' value="<?= $_GET['redirect_to']; ?>" >
				<?php else: ?>
					<input type='hidden' name='fbl_redirect_to' value="<?= home_url('focus-messages') ?>" >
				<?php endif; ?>

				<div class="form-group">
					<label for="fbl_emailaddress">Email address</label>
					<input type="email" class="form-control" id="fbl_emailaddress" name="fbl_emailaddress" required>
				</div>
				<div class="form-group">
					<label for="fbl_password">Password</label>
					<input type="password" class="form-control" id="fbl_password" name="fbl_password" required>
				</div>
				<div class="form-group">
					<div class="form-check">
						<input type="checkbox" class="form-check-input" id="fbl_remember" name="fbl_remember">
						<label class="form-check-label" for="fbl_remember">Remember Me</label>
					</div>
				</div>
				<button type="submit" name="fbl_submit" class="btn btn-primary">Login</button>
				
			</form>
			
		</div>
		<?php
		return ob_get_clean();		

	}

	/**
	 * override custom post type archive with the plugin's archive file.
	 *
	 * @param	string		$template
	 * @since    1.0.0
	 */
	public function focusboard_override_tax_template( $template ){

		if(is_archive('messages')):
			$template = plugin_dir_path( __FILE__ ) . 'templates/archive-message.php';
		endif;

        return $template;

	}

	/**
	 * markup and code for the [focusboard_new_message] shortcode.
	 *
	 * @param	array		$atts
	 * @param	string		$content
	 * @since    1.0.0
	 */
	public function focusboard_new_message_form_function( $atts, $content = "" ){
		ob_start();

		$current_user = wp_get_current_user();

		?>
		<div class='focusboard-new-message'>

			<h4 class='focusboard-new-message-title'>Add a new message</h4> 

			<div class='focusboard-new-message-loading'>
				<div class="d-flex justify-content-center">
					<div class="spinner-grow" role="status">
						<span class="sr-only">Loading...</span>
					</div>
				</div>
			</div>

			<form id="focusboard-new-message-form" action="" method="post">

				<input type='hidden' name='fbnm_user_id' value="<?= $current_user->ID; ?>">
				<div class="form-group">
					<label for="fbnm_title">Message Title</label>
					<input type="text" class="form-control" id="fbnm_title" name="fbnm_title" required>
				</div>
				<div class="form-group">
					<label for="fbnm_desc">Message Description</label>
					<textarea class="form-control" id="fbnm_desc" name="fbnm_desc" cols="100%" rows="10" required></textarea>
				</div>
				<button type="submit" name="fbnm_submit" class="btn btn-primary">Add Message</button>

			</form>

			<div class='focusboard-success-screen'>
				<div class='focusboard-success-screen-box'>
					<div class='focusboard-success-screen-icon'><svg aria-hidden="true" focusable="false" data-prefix="fad" data-icon="check-circle" class="svg-inline--fa fa-check-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><g class="fa-group"><path class="fa-secondary" fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm155.31 195.31l-184 184a16 16 0 0 1-22.62 0l-104-104a16 16 0 0 1 0-22.62l22.62-22.63a16 16 0 0 1 22.63 0L216 308.12l150.06-150.06a16 16 0 0 1 22.63 0l22.62 22.63a16 16 0 0 1 0 22.62z" opacity="0.4"></path><path class="fa-primary" fill="currentColor" d="M227.31 387.31a16 16 0 0 1-22.62 0l-104-104a16 16 0 0 1 0-22.62l22.62-22.63a16 16 0 0 1 22.63 0L216 308.12l150.06-150.06a16 16 0 0 1 22.63 0l22.62 22.63a16 16 0 0 1 0 22.62l-184 184z"></path></g></svg></div>
					<div>New message added successfully</div>
				</div>
			</div>
		</div>
		<?php
		return ob_get_clean();		


	}

	/**
	 * override the comments template.
	 *
	 * @param	string		$template
	 * @since    1.0.0
	 */
	public function focusboard_comments_template( $template ) {
		return plugin_dir_path( __FILE__ ).'templates/comments.php';
	}

	/**
	 * ajax handler to add a new message to the database.
	 *
	 * @since    1.0.0
	 */
	public function focusboard_add_message_handler(){

		$response = false;
		// Create new message args
		$args = array(
			'post_type' 	=> 'message',
			'post_title'    => wp_strip_all_tags( $_POST['data'][1]['value'] ),
			'post_content'  => $_POST['data'][2]['value'],
			'post_status'   => 'publish',
			'post_author'   => $_POST['data'][0]['value'],
		);
		
		// Insert the new message into the database
		$pid = wp_insert_post( $args );
		if(!is_wp_error($pid)){
			$response = json_encode(array(
				'id' => $pid,
				'title' => $_POST['data'][1]['value'],
				'desc' => apply_filters('the_content', $_POST['data'][2]['value']),
				'link' => get_permalink($pid),
			));
		} else {
			error_log( print_r( $pid, true ));
		}

		echo print_r($response, true);

		wp_die();

	}



}
